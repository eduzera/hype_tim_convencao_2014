jQuery(document).ready(function($) {
  new agenda($('#agenda-content'));
});

var agenda = function($container){
  var _this = this;
  this.$container = $container;

  this.startup = function(){
    if(!this.$container[0]){ return false; }

    this.$container.on('click', 'a.title', function(event) {
      event.preventDefault();
      
      _this.manageDate($(this));
    });
  }

  this.manageDate = function($link){
    var $section = $link.closest('section');
    var $header  = $section.find('header');

    if($header.hasClass('active')){
      $header.removeClass('active');
      this.changeArrowImg($section, false);
      this.closeDate($section);
    }else{
      $header.addClass('active');
      this.openDate($section);
      this.changeArrowImg($section, true);
      this.scrollToOpenSection($section);
    }
  },

  this.openDate = function($section){
    $section.find('article').fadeIn(200);
  },

  this.closeDate = function($section){
    $section.find('article').fadeOut(200);
  },

  this.scrollToOpenSection = function($section){
    $("html, body").stop(true).animate({scrollTop: $section.offset().top}, 500);
  },

  this.changeArrowImg = function($section, is_opened){
    var $img = $section.find('header .arrow img');

    if(is_opened){
      $img.attr('src', 'img_dna/2014/agenda/arrow-down-white.png');
    }else{
      $img.attr('src', 'img_dna/2014/agenda/arrow-down.png');
    }
  }

  this.startup();
}


<?
include("include/dias.php");

session_start();
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
        
        <link rel="stylesheet" href="css_dna/normalize.min.css">
        <link rel="stylesheet" href="css_dna/main.css">
        <link rel="stylesheet" href="css_dna/fonts.css">
        <link rel="stylesheet" href="css_dna/login.css">
        <link rel="stylesheet" href="css_dna/login-mobile.css">

        <script src="js_dna/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script>
			function addInputSubmitEvent(form, input) {
				input.onkeydown = function(e) {
					e = e || window.event;
					if (e.keyCode == 13) {
						form.submit();
						return false;
					}
				};
			}
			
			window.onload = function() {
				var forms = document.getElementsByTagName('form');
			
				for (var i=0;i < forms.length;i++) {
					var inputs = forms[i].getElementsByTagName('input');
			
					for (var j=0;j < inputs.length;j++)
						addInputSubmitEvent(forms[i], inputs[j]);
				}
			};
		</script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <section id='container'>
          <div id='body'>
            <h1 id='dna-tim'><img src="img_dna/login/dna-tim-logo.png" alt='DNA TIM - Nosso Time Nossa Força'></h1>

            <section id='access-bnt'>
                <a href="javasctipt://"> <img src="img_dna/login/bnt-access.png"></a>
            </section>

            <section id='login-form'>
                <form action="validate2.php" method="get" name="form1" id="form1">
						<p>
                        <label>cpf</label> 
                        <input name="cpf" type="text" size="11" maxlength="11"  />
                        <span>(somente números)</span>
                        </p>
                        <p>
                        <label>gsm</label>
    					<input name="Celular" type="text" size="11" maxlength="11" />  
    					<span>(ex: 2112345678)</span>
                    	</p>
                    <input type="submit" style="" value="Entrar" />
    
        			</form>
                    <p>N&atilde;o consegue acessar? <a href="mailto:duvidas@dnatim.com.br" target="_blank"> Clique aqui</a> e envie-nos um e-mail</p>
            </section> 
          </div>

          <footer>
              <h2>Convenção de Vendas | <span>Abril 2013</span> - Bahia</h2>
              <figure>
                  <img src="img_dna/login/tim-logo.png">
              </figure>  
          </footer>

          <div id='form-line'         class='layout-element'></div>
          <div id='details-top-left'  class='layout-element'> 
              <img src="img_dna/login/top-left-detail.png">
          </div>
        </section>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js_dna/vendor/jquery-1.9.1.min.js"><\/script>')</script>

        <script src="js_dna/vendor/jquery.backstretch.min.js"></script>
        <script src="js_dna/login.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>


<!-- Inicio Relogio-->

<table width="209" border="0" cellspacing="0" cellpadding="0" style="display:none">
      <tr>
        <td width="96" rowspan="3" align="center" valign="middle"><? if($dias > 0){ ?>
          <img src="img/data_01.png" width="83" height="83" />
          <? } ?></td>
        <td width="113" height="31" align="center"><? if($dias > 0){ ?>
          <img src="img/data_02.png" width="82" height="21" />
          <? } ?></td>
      </tr>
      <tr>
        <td height="74" align="center"><? if($dias == 28){ ?>
          <img src="img/28.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 27){ ?>
          <img src="img/27.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 26){ ?>
          <img src="img/26.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 25){ ?>
          <img src="img/25.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 24){ ?>
          <img src="img/24.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 23){ ?>
          <img src="img/23.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 22){ ?>
          <img src="img/22.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 21){ ?>
          <img src="img/21.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 20){ ?>
          <img src="img/20.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 19){ ?>
          <img src="img/19.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 18){ ?>
          <img src="img/18.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 17){ ?>
          <img src="img/17.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 16){ ?>
          <img src="img/16.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 15){ ?>
          <img src="img/15.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 14){ ?>
          <img src="img/14.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 13){ ?>
          <img src="img/13.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 12){ ?>
          <img src="img/12.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 11){ ?>
          <img src="img/11.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 10){ ?>
          <img src="img/10.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 9){ ?>
          <img src="img/09.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 8){ ?>
          <img src="img/08.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 7){ ?>
          <img src="img/07.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 6){ ?>
          <img src="img/06.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 5){ ?>
          <img src="img/05.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 4){ ?>
          <img src="img/04.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 3){ ?>
          <img src="img/03.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 2){ ?>
          <img src="img/02.png" width="94" height="62" />
          <? } ?>
          <? if($dias == 1){ ?>
          <img src="img/01.png" width="94" height="62" />
          <? } ?></td>
      </tr>
      <tr>
        <td height="38" align="center"><? if($dias > 0){ ?>
          <img src="img/data_03.png" width="54" height="21" />
          <? } ?></td>
      </tr>
    </table>

<!-- Fim Relogio -->
<?
session_destroy();
?>
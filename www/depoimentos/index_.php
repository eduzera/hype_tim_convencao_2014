<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>TIM - Convenção Nacional de Vendas 2014</title>

        <!--[if lt IE 9]>
            <script type="text/javascript" src="assets/js/html5.js"></script>
        <![endif]-->

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="assets/stylesheets/reset.css">
        <link rel="stylesheet" href="assets/stylesheets/application.css">

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header id="main">
            <h1>
                <a href="/" title=""> <img src="assets/images/logo.png" alt="TIM"> </a>
            </h1>
        </header><!-- /main -->

        <section class='basic-tricks'>
            <h2>Nossa convenção nacional de vendas será em Abril e você pode <br/>
            dar o seu recado. Grave um vídeo de até 10 segundos <br/>
            respondendo: <span>O que é ser consumer?</span> <br/>
            Encha o peito e encerre com o nosso grito de guerra!
            </h2>

            <h3>
              Siga essas dicas báscias para gravar melhor seu vídeo:
            </h3>

            <ul class='tricks'>
              <li> <img src="assets/images/trick_01.jpg" alt="Dica 01"> </li>
              <li> <img src="assets/images/trick_02.jpg" alt="Dica 02"> </li>
              <li> <img src="assets/images/trick_03.jpg" alt="Dica 03"> </li>
              <li> <img src="assets/images/trick_04.jpg" alt="Dica 04"> </li>
              <li> <img src="assets/images/trick_05.jpg" alt="Dica 05"> </li>
              <li> <img src="assets/images/trick_06.jpg" alt="Dica 06"> </li>
            </ul>
        </section>

        <section class='form'>
          <form action="/action-url" method="post" accept-charset="utf-8">
            <article>
              <h4>Formulário</h4>
              <p class='user-name'>
                <label for="user-name">Nome</label>
                <input id="user-name" type="text" name="">
              </p>
              <p class='user-regional'>
                <label for="user-regional">Regional</label>
                <input id="user-regional" type="text" value="">
              </p>
              <p class='user-gsm'>
                <label for="user-gsm">Gsm</label>
                <input id="user-gsm" type="text" value="">
              </p>
              <p class='user-email'>
                <label for="user-email">Email</label>
                <input id="user-email" type="email" value="">
              </p>
            </article>
            <section class='upload-area'>
              <article>
                <div class='field-customized'>
                  <input type="file" multiple="multiple" name="file" id="file_upload">
                  <span>
                    <img src="assets/images/upload_bnt.jpg" alt="" />
                  </span>
                </div>
                <h3>Clique ao lado para subir seu vídeo <br/> 
                e espere a mensagem de confirmação.</h3>
              </article>
            </section>
          </form>
        </section>

        <footer>
          Copyright TIM Celular 2013 - Todos os direitos reservados.
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

        <script src="assets/js/main.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>

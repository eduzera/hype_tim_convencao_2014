function FilterNumberAndChar(e)
{
	if (document.all) // Internet Explorer
		var tecla = event.keyCode;
	else if(document.layers) // Nestcape
		var tecla = e.which;

	if ((tecla > 47 && tecla < 58) || (tecla >= 65 && tecla <= 90) || (tecla >= 97 && tecla <= 122)) // numeros de 0 a 9
		return true;
	else{
		if (tecla != 8) // backspace
			event.keyCode = 0;
			//return false;
		else
			return true;
	}
}

function GetStyle(whichLayer) {
	if (document.getElementById) {
		var objStyle = document.getElementById(whichLayer).style;
	} else if (document.all) {
		var objStyle = document.all[whichLayer].style;
	} else if (document.layers) {
		var objStyle = document.layers[whichLayer].style;
	}
	
	return objStyle;
} 

function fcTextLength (myText, myMessage) {
	if (myText.value.length == 0) { 
		alert(myMessage);
		if (myText.type != "hidden") {
			myText.focus();
		} 
		return false;
	} else {
		return true;
	}
}

function fcIsValidDate(c_day, c_month, c_year, myMessage) {
	if(c_day.value == '')
	{
		alert("Por favor, informe o dia " + myMessage + ".");
		c_day.focus();
		return false;
	}
	
	if(c_day.value.length == 1) c_day.value = '0'+c_day.value;
	re = /^\d{2}/;
	if (!re.test(c_day.value))
	{
		alert("Por favor, informe somente n�meros no dia " + myMessage + ".");
		c_day.focus();
		return false;
	}
	if(c_month.value == '')
	{
		alert("Por favor, informe o m�s " + myMessage + ".");
		c_month.focus();
		return false;
	}
	if(c_month.value.length == 1) c_month.value = '0'+c_month.value;
	re = /^\d{2}/;
	if (!re.test(c_month.value))
	{
		alert("Por favor, informe somente n�meros no m�s " + myMessage + ".");
		c_month.focus();
		return false;
	}
	if(c_year.value == '')
	{
		alert("Por favor, informe o ano " + myMessage + ".");
		c_year.focus();
		return false;
	}
	re = /^\d{4}/;
	if (!re.test(c_year.value))
	{
		alert("Por favor, informe somente n�meros no ano " + myMessage + ".");
		c_year.focus();
		return false;
	}
	var ichkdt = iConsisteData(parseInt(c_day.value,10),parseInt(c_month.value,10),parseInt(c_year.value,10));
	if(ichkdt==1)
	{
		alert("Por favor, informe corretamente o dia " + myMessage + ".");
		c_day.focus();
		return false;
	}
	if(ichkdt==2)
	{
		alert("Por favor, informe corretamente o m�s " + myMessage + ".");
		c_month.focus();
		return false;
	}
	if(ichkdt==3)
	{
		alert("Por favor, informe corretamente o ano " + myMessage + ".");
		c_year.focus();
		return false;
	}
	if(ichkdt==4)
	{
		alert("O dia e o m�s " + myMessage + " est�o inconsistentes.");
		c_day.focus();
		return false;
	}
	
	return true;
}


function fcSelect(mySelect, myMessage) {
	count = false;
	for (i=0;i<mySelect.options.length;i++) {
		if (mySelect.options[i].selected) {
			if (mySelect.options[i].value!="" && mySelect.options[i].value!="0") {
				count = true;
			}
		}
	}
	if(!count) {
		alert(myMessage);
	}
	
	return count;
}

function fcTextIsEmail(myText, myMessage) {
	checkEmail = myText.value;
	if ((checkEmail.indexOf('@') < 0) || ((checkEmail.charAt(checkEmail.length-4) != '.') && (checkEmail.charAt(checkEmail.length-3) != '.'))) {
		alert(myMessage);
		myText.select();
		return false;
	} else {
		return true; 
	} 
} 

function iConsisteData(intDD, intMM, intAA)
{
	if ( (intDD + intMM + intAA) > 0 ) {
		if (intAA < 1) {
			return(3);
		}
		if (intMM < 1 || intMM > 12) {
			return(2);
		}	
		if (intDD < 1 || intDD > 31) {
			return(1);
		} 
		if ((intMM == 4 || intMM == 6 || intMM == 9 || intMM == 11) && intDD > 30) {
			return(4);
		} else if (intMM == 2) {

			if ( intAA % 4 > 0   &&  intDD > 28 ) {
				return(4);
			} else if ( intDD > 29 ) {
				return(4);
			}
		}
	} else {
		return(1)
	}
	return(0)
}

function scrollToAnchor(aname)
{
	var anchors, i, ele;

	if (!document.getElementById)
		return;
	
	// get anchor
	anchors = document.getElementsByTagName("a");
	for (i=0;i<anchors.length;i++) {
		if (anchors[i].name == aname) {
			ele = anchors[i];
			i = anchors.length;
		}
	}
	
	// set scroll target
	if (window.scrollY)
		scrSt = window.scrollY;
	else if (document.documentElement.scrollTop)
		scrSt = document.documentElement.scrollTop;
	else
		scrSt = document.body.scrollTop;

	
	
	scrDist = ele.offsetTop - scrSt;
	scrDur = 500;
	scrTime = 0;
	scrInt = 10;
	
	// set interval
	clearInterval(scrollInt);
	scrollInt = setInterval( scrollPage, scrInt );
}
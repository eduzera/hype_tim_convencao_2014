﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class depoimentos_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
        MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand();

        string SQL;

        conn.ConnectionString = "server=mysql.tudoseconecta.com.br; userid=tudoseconecta;password=MySQLTim;database=tudoseconecta;";
        conn.Open();
        try
        {
            if (file_upload.HasFile)
            {

                if (file_upload.PostedFile.ContentLength < 4194304 )
                {
                    string caminho = Server.MapPath("files");
                    file_upload.SaveAs(caminho + "/" + file_upload.FileName);

                    SQL = "insert into depoimentos(nome,regional,gsm,email,file) values(@nome,@regional,@gsm,@email,@file)";
                    cmd.Connection = conn;
                    cmd.Parameters.Add("@nome", userName.Text);
                    cmd.Parameters.Add("@regional", userRegional.Text);
                    cmd.Parameters.Add("@gsm", userGsm.Text);
                    cmd.Parameters.Add("@email", userEmail.Text);
                    cmd.Parameters.Add("@file", file_upload.FileName);

                    cmd.CommandText = SQL;
                    cmd.ExecuteNonQuery();
                    ClientScript.RegisterStartupScript(this.GetType(), "error", "<script>alert('Envio realizado com sucesso. obrigado por enviar seu depoimento!');</script>");
                }
                else
                {
                ClientScript.RegisterStartupScript(this.GetType(), "error", "<script>alert('Arquivo selecionado muito grande, porfavor compacte seu video para que tenha até 5 MB. Tamanho atual:" + file_upload.PostedFile.ContentLength + "');</script>");
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "error", "<script>alert('Por favor selecione um arquivo!!!');</script>");
            }
        }
        catch (Exception)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "error", "<script>alert('Arquivo selecionado muito grande, porfavor compacte seu video para que tenha até 5 MB. Tamanho atual:" + file_upload.PostedFile.ContentLength + "');</script>");
        }
    }

}
<?
include("include/connect.inc.php");
$mpz = "home";
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        
        <link rel="stylesheet" href="css_dna/normalize.min.css">
        <link rel="stylesheet" href="css_dna/main.css">
        <link rel="stylesheet" href="css_dna/fonts.css">
        <link rel="stylesheet" href="css_dna/application.css">
        <link rel="stylesheet" href="css_dna/menu_left.css">
        <link rel="stylesheet" href="css_dna/mobile.css">
		<!-- ... já existentes - css files ... -->
        <link rel="stylesheet" href="css_dna/jquery.mCustomScrollbar.css">
        <script src="js_dna/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="js/script_busca.js"></script> 
		<script> 
        function pesquisa(Find) {
        url="busca_nome.php?mpz=<?=$mpz?>&Find="+Find; 
        ajax(url); 
        
        if (document.formScroll.Find.value == "") {
            window.location.href = 'amigos.php';	
            } 
        }
        </script> 

    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <header id='menu-mobile' class='clearfix'>
            <h2>Amigos</h2>
            <nav>
                <ul>
                    <li>
                        <a href="javascript://" class='link open'><img src="img_dna/menu_mobile/open.png"></a>
                        <a href="javascript://" class='link close'><img src="img_dna/menu_mobile/close.png"></a>
                        <ul class='dropdown'>
                            <li><a href="local.php">Local  </a></li>
                            <li><a href="agenda.php">Agenda </a></li>
                            <li><a href="perfil.php">Perfil </a></li>
                            <li><a href="">Amigos </a></li>
                            <li><a href="home.php">Mural  </a></li>
                            <li><a href="duvidas.php">Dúvidas</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
        <section id='container'>
            <ul id='menu-left'>
                <li class="ico-perfil">
                    <a href="perfil.php" title="Perfil">Perfil</a>
                </li>
                <li class="ico-agenda">
                    <a href="agenda.php" title="Agenda">Agenda</a>
                </li>
                <li class="ico-mural">
                    <a href="home.php" title="Mural">Mural</a>
                </li>
                <li class="ico-fotos">
                    <a href="fotos.php" title="Fotos">Fotos</a>
                </li>
                <li class="ico-amigos">
                    <a href="amigos.php" title="Amigos" class='active'>Amigos</a>
                </li>
                <li class="ico-locais">
                    <a href="local.php" title="Local">Local</a>
                </li>
            </ul>

            <h1 class='dna-logo'>
                <a href="perfil.php" title="Perfil">
                    <img src="img_dna/2014/dna-tim-logo.png" alt="DNA TIM LOGO">
                </a>

                <img src="img_dna/2014/4g_blue.png" alt="4g", class='icon-4g'>
            </h1>
            <section id='content'>
                <article class='clearfix'>
                <section id='search-fireds' class='info'>  
                <form id="formScroll" name="formScroll" method="post" action="">
                   <article id='search-friends'>
                          <input name="sn" type="hidden" id="sn" value="<?=$sn?>" />
                          <input name="mpz" type="hidden" id="mpz" value="<?=$mpz?>" />
                          <input name="Find" type="text" onkeyup="pesquisa(this.value)" id="Find" value="" size="30" autocomplete="off" />
                          &nbsp;<a href="amigos.php?mpz=<?=$mpz?>" target="_parent">Ver Todos</a>
                    </article>      
                    </form>
                 </section>   
                 </article>
                <section id='friends-list' class='with-scroll'>
                    <div id="pagina">
    			<?

    			$sel_ordem = "SELECT * FROM perfil WHERE perfil.$mpz = '$mpz' ORDER BY perfil.nome DESC";
    			$res_ordem = mysql_query($sel_ordem);
    			while($lin_ordem = mysql_fetch_array($res_ordem)){
    				$z++;			
    				$id = $lin_ordem['id'];
    				$nome = $lin_ordem['nome']; 
    				$ord = $lin_ordem['ordemdef']; 
    	
    					$upd_ordem = "UPDATE perfil SET perfil.ordemdef = '$z' WHERE perfil.nome = '$nome'";
    					mysql_query($upd_ordem);
    				}
    				if($sn == ""){				
    				$sel_full = "SELECT * FROM perfil WHERE perfil.$mpz = '$mpz' and perfil.confirmado = '1' ORDER BY perfil.ordemdef DESC LIMIT 0, 60";
    				}else{				
    				$sel_full = "SELECT * FROM perfil WHERE perfil.ordemdef = '$sn' and perfil.$mpz = '$mpz' and perfil.confirmado = '1'";
    				}				
    				//echo $sel_full;				
    				$res_full = mysql_query($sel_full);
    					while($lin_full = mysql_fetch_array($res_full)){
    						$tu = $lin_full['ordemdef'];
    						$uc = $lin_full['id'];
    						$Image = $lin_full['img_name'];
    						$Nome = utf8_encode($lin_full['nome']);						
    						$nome = explode(" ", $Nome);
    						$email = $lin_full['email'];		
    						$Mial = $lin_full['celular'];
    			?>
    				<div id="<?=$tu?>">
    					<article>
    			
                            <figure class='user-picture'>
                              <? if($Image != "" and file_exists("pics2/$Image")){ ?>
                              <img src="pics2/<?=$Image?>" alt="valor do alt"  title="<?=$Nome?>" />
                              <? }else{ ?>
                              <img src="img/foto_defaut.jpg" alt="valor do alt" title="<?=$Nome?>" />
                              <? } ?>
                            </figure>
                            <!-- <? if($Mial != "1234567890"){ ?><a href="mailto:<?=$email?>" target="_parent"><? } ?><img src="img/envelope.jpg" alt="" width="20" height="14" border="0" /></a></td> -->
                            <a href="mailto:<?=$email?>" target="_parent"><? echo $Nome; ?></a>
                          </article>
    				</div>                    
                <? } ?>            
                
                    </div>
                </section>
                <section id='user-info-thumb' style="display:none">
                    <figure class='user-picture'>
                        <img src="img_dna/placeholders/profile01.png">
                    </figure>
                    <p> <span>Nome:</span> Ricardo Pacheco </p>
                    <p> <span>Cidade:</span> Rio de Janeiro </p>
                    <p> <span>Estado:</span> RJ </p>
                </section>

                <div id='tim-logo'          class='layout-element'>
                    <img src="img_dna/2014/tim-logo.png">
                </div>
            </section>
        </section>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js_dna/vendor/jquery-1.9.1.min.js"><\/script>')</script>
		<script src="js_dna/vendor/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js_dna/vendor/jquery.backstretch.min.js"></script>
        <script src="js_dna/main.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>

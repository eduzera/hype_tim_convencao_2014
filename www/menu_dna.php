<?
include("include/connect.inc.php");
include("include/check_session2.php");  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        
        <link rel="stylesheet" href="css_dna/normalize.min.css">
        <link rel="stylesheet" href="css_dna/main.css">
        <link rel="stylesheet" href="css_dna/fonts.css">
        <link rel="stylesheet" href="css_dna/menu.css">
    <!-- ... já existentes - css files ... -->
        <link rel="stylesheet" href="css_dna/jquery.mCustomScrollbar.css">
        <script src="js_dna/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <section id='main-menu'>
          <header>
            <h1>
              <a href="menu_dna.php" title="DNA #tudoseconecta"> <img src="img_dna/2014/menu/logo-tim.png" alt=""></a>
            </h1>
            <figure class='icon-4g'>
               <img src="img_dna/2014/menu/4g-icon.png" alt="4G">
             </figure> 
          </header><!-- /header -->
          <article> 
            <a href="perfil.php"    class='perfil' title="Perfil">Perfil</a>
            <a href="home.php"      class='mural'  title="Mural">Mural</a>
            <a href="amigos.php"    class='amigos' title="Amigos">Amigos</a>
            <a href="agenda.php"    class='agenda' title="Agenda">Agenda</a>
            <a href="fotos.php"     class='fotos'  title="Fotos">Fotos</a>
            <a href="local.php"     class='local'  title="Local">Local</a>
          </article>
        </section>
        

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js_dna/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js_dna/vendor/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js_dna/vendor/jquery.backstretch.min.js"></script>
        <script src="js_dna/menu.js"></script>

        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class depoimentos_Downloads : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection();
            MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand();
            MySqlDataReader dr;


            string SQL;

            conn.ConnectionString = "server=mysql.tudoseconecta.com.br; userid=tudoseconecta;password=MySQLTim;database=tudoseconecta;";
            conn.Open();
            try
            {
                SQL = "select nome,regional,gsm,email,file from depoimentos";
                cmd.Connection = conn;
                cmd.CommandText = SQL;
                dr = cmd.ExecuteReader();
                litFiles.Text= "<table border=1><tr><td>Nome</td><td>Regional</td><td>GSM</td><td>Email</td><td>Video</td></tr>";
                while (dr.Read())
                {
                    litFiles.Text += "<tr><td>" + dr["nome"].ToString() + " </td>";
                    litFiles.Text += "<td>" + dr["regional"].ToString() + " </td>";
                    litFiles.Text += "<td>" + dr["gsm"].ToString() + " </td>";
                    litFiles.Text += "<td>" + dr["email"].ToString() + " </td>";
                    litFiles.Text += @"<td><a href='\files\" + dr["file"].ToString() + "' target='_blank'>" + dr["file"].ToString() + " </a></td></tr>";
                }
                litFiles.Text += "</table>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
jQuery(document).ready(function($) {
  // get_window_info();
  $.backstretch("img_dna/2014/background.jpg");

  control_menu_mobile();

  openComment();

  setContentHeight();

});

function get_window_info(){
  var width  = $(window).width();
  var height = $(window).height();

  $('body').append(width + "x" + height);
}

function openComment(){
  var $comment_link = $('a.comment');

  $comment_link.on('click', function(event) {
    event.preventDefault();

    $(this).parent().find('.post-comment:first').show();
  });
}


function control_menu_mobile(){
  var $menu       = $('#menu-mobile');
  var $menu_drop  = $menu.find('ul.dropdown');
  var $open_link  = $menu.find('a.open');
  var $close_link = $menu.find('a.close').not('.submenu');

  $open_link.on('click', function(event) {
      event.preventDefault();
      $menu_drop.not('.submenu').fadeIn('fast');
      $open_link.hide();
      $close_link.show();
  });

  $close_link.on('click', function(event) {
      event.preventDefault();
      $menu_drop.fadeOut('fast');
      $close_link.hide();
      $open_link.show();
  });

  $menu.find('.submenu').each(function(index) {
    control_submenu($(this));
  });
}

function control_submenu(submenu){
  var $submenu       = submenu;
  var $link_to_menu  = $submenu.parents('#menu-mobile').find("a[data-submenu='"+$submenu.attr('data-parent')+"']");
  var $link_to_close = $submenu.parents('#menu-mobile').find('a.close.submenu');
  var $open_link     = $submenu.parents('#menu-mobile').find('a.open');

  $link_to_menu.on('click', function(event) {
    event.preventDefault();

    $open_link.hide();
    
    $link_to_close.show();

    $submenu.fadeIn('fast');
  });

  $link_to_close.on('click', function(event) {
    event.preventDefault();
    
    $open_link.show();

    $link_to_close.hide();

    $submenu.fadeOut('fast');
  });
}

function setContentHeight(){
  var window_height   = $(window).height();
  var $scroll_element = $(".with-scroll");
  var scroll_height   = undefined;

  if($('#menu-mobile').css('display') === 'none' && $scroll_element[0]){
    scroll_height = window_height - $scroll_element.offset().top;

    $scroll_element.height(scroll_height);

    setTimeout(add_scroll, 1 * 1000);
  }
}

function add_scroll(){
  var $scroll_element = $(".with-scroll");

  $scroll_element.mCustomScrollbar({
      scrollInertia: 200,
      theme: "dark",
      scrollButtons:{
        enable: true,
        scrollSpeed: 100
      },
      advanced:{
        updateOnBrowserResize: true
      },
      autoHideScrollbar: true
    });
}
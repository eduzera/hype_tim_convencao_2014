﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="depoimentos_Default" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>TIM - Convenção Nacional de Vendas 2014</title>

    <!--[if lt IE 9]>
            <script type="text/javascript" src="assets/js/html5.js"></script>
        <![endif]-->

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="assets/stylesheets/reset.css">
    <link rel="stylesheet" href="assets/stylesheets/application.css">

    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
</head>
<body>
    
    <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

    <header id="main">
        <h1>
            <a href="/" title="">
                <img src="assets/images/logo.png" alt="TIM" width = "160">
            </a>
        </h1>
    </header>
    <!-- /main -->

    <section class='basic-tricks'>
        <h2>Nossa convenção nacional de vendas será em Abril e você pode
            <br />
            dar o seu recado. Grave um vídeo de até 10 segundos
            <br />
            respondendo: <span>O que é ser consumer?</span>
            <br />
            Encha o peito e encerre com o nosso grito de guerra!
            </h2>

        <h3>Siga essas dicas básicas para gravar melhor seu vídeo:
            </h3>

        <ul class='tricks'>
            <li>
                <img src="assets/images/trick_01.jpg" alt="Dica 01" width="900">
            </li>
            <li>
                <img src="assets/images/trick_02.jpg" alt="Dica 02" width="900">
            </li>
            <li>
                <img src="assets/images/trick_03.jpg" alt="Dica 03" width="900">
            </li>
            <li>
                <img src="assets/images/trick_04.jpg" alt="Dica 04" width="900">
            </li>
            <li>
                <img src="assets/images/trick_05.jpg" alt="Dica 05" width="900">
            </li>
            <li>
                <img src="assets/images/trick_06.jpg" alt="Dica 06" width="900">
            </li>
        </ul>
    </section>

    <section class='form'>
       <form id="form1" runat="server">
            <article>
                <h4>Formulário</h4>
                <p class='user-name'>
                    <label for="user-name">Nome</label>
                    <asp:TextBox ID="userName" runat="server"></asp:TextBox>
                    <!--<input id="user-name" type="text" name="">-->
                </p>
                <p class='user-regional'>
                    <label for="user-regional">Regional</label>
                    <asp:TextBox ID="userRegional" runat="server"></asp:TextBox>
                    <!--<input id="user-regional" type="text" value="">-->
                </p>
                <p class='user-gsm'>
                    <label for="user-gsm">Gsm</label>
                    <asp:TextBox ID="userGsm" runat="server"></asp:TextBox>
                    <!--<input id="user-gsm" type="text" value="">-->
                </p>
                <p class='user-email'>
                    <label for="user-email">Email</label>
                    <asp:TextBox ID="userEmail" type="email"  runat="server"></asp:TextBox>
                    <!--<input id="user-email" type="email" value="">-->
                </p>
                <p class='user-file'>
                    <label for="user-name">video</label>
                    <asp:FileUpload runat="server" ID="file_upload" />
                </p>
            </article>
            <section class='upload-area'>
                <article>
                    <div class='field-customized'>
                        <span>
                            <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" ImageUrl="assets/images/upload_bnt.jpg" />
                        </span>
                    </div>
                    <h3>Clique ao lado para subir seu vídeo
                        <br />
                        e espere a mensagem de confirmação.</h3>
                    
                </article>
            </section>
        </form>
    </section>

    <footer>
        Copyright TIM Celular 2013 - Todos os direitos reservados.
       
    </footer>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

    <script src="assets/js/main.js"></script>

    <script>
        var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
        (function (d, t) {
            var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
            g.src = '//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g, s)
        }(document, 'script'));
        </script>
</body>
</html>
